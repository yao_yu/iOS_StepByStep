//
//  ViewController.swift
//  P010_Navigation
//
//  Created by yao_yu on 14-9-23.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView:UITableView!
    
    var recipeName = [String]()
    var thumbnail = [String]()
    var prepTime = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        loadData()
    }
    
    func loadData(){
        var recipes = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("recipes", ofType: ".plist")!)
        recipeName = recipes["RecipeName"] as [String]
        thumbnail = recipes["Thumbnail"] as [String]
        prepTime = recipes["PrepTime"] as [String]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return SimpleTableCell.height
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipeName.count
    }
    
    // 删除行
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        recipeName.removeAtIndex(indexPath.row)
        prepTime.removeAtIndex(indexPath.row)
        thumbnail.removeAtIndex(indexPath.row)
        
        tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("SimpleTableCell") as? SimpleTableCell
        if cell == nil{
            var nib = NSBundle.mainBundle().loadNibNamed("SimpleTableCell", owner: self, options: nil)
            cell = nib[0] as? SimpleTableCell
//            cell = SimpleTableCell()
        }
        
        var row = indexPath.row
        cell?.lblName.text = recipeName[row]
        cell?.lblTime.text = prepTime[row]
        cell?.thumbnailImage.image = UIImage(named: thumbnail[row])
        
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        return cell!
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "TableCellToOther" {
            var path = tableView.indexPathForSelectedRow()
            var row = path?.row
            var title:String = "第\(path?.section)组, \(recipeName[row!])"
            (segue.destinationViewController as DetailViewController).showTitle(title)
        }
        
//        if([segue.identifier isEqualToString:@"TableCellToOther"])
//        {
//            NSIndexPath* path = [self.tableView indexPathForSelectedRow];
//            NSInteger row = path.row;
//            NSString* title = [NSString stringWithFormat:@"第%d组，%@",path.section,[array objectAtIndex:row]];
//            //可以通过 segue.destinationviewcontroller获得跳转的下一个controller  可以直接调用其方法
//            //记得 加上头文件      #import "otherController.h"
//            [segue.destinationViewController showTitle:title];
//        }
    }
}

