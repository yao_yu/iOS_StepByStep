//
//  DetailViewController.swift
//  P025_SplitView
//
//  Created by yao_yu on 14-9-23.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var detailWebView: UIWebView!



    var detailItem: (String,String)? {
        didSet {
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail: (String,String) = self.detailItem {
            let (title, urlString) = detail
            if let label = self.detailDescriptionLabel {
                label.text = title
            }

            if let detailWebView = self.detailWebView{
                self.navigationItem.title = title
                
                let url = NSURL(string: urlString)
                let request = NSURLRequest(URL: url)

                detailWebView.loadRequest(request)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        detailWebView.delegate = self
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

