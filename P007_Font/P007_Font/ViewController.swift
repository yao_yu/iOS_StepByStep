//
//  ViewController.swift
//  P007_Font
//
//  Created by yao_yu on 14-9-22.
//  Copyright (c) 2014年 yao_yu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView:UITableView!
    
    var recipeName = [String]()
    var thumbnail = [String]()
    var prepTime = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        loadData()
    }

    func loadData(){
        var recipes = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("recipes", ofType: ".plist")!)
        recipeName = recipes["RecipeName"] as [String]
        thumbnail = recipes["Thumbnail"] as [String]
        prepTime = recipes["PrepTime"] as [String]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return SimpleTableCell.height
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipeName.count
    }
    
    // 删除行
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        recipeName.removeAtIndex(indexPath.row)
        prepTime.removeAtIndex(indexPath.row)
        thumbnail.removeAtIndex(indexPath.row)
        
        tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("SimpleTableCell") as? SimpleTableCell
        if cell == nil{
            var nib = NSBundle.mainBundle().loadNibNamed("SimpleTableCell", owner: self, options: nil)
            cell = nib[0] as? SimpleTableCell
        }
        
        var row = indexPath.row
        cell?.lblName.text = recipeName[row]
        cell?.lblTime.text = prepTime[row]
        cell?.thumbnailImage.image = UIImage(named: thumbnail[row])
        
        return cell!
    }
}

